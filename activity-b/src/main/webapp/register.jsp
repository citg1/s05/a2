<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
	<head>
	<meta charset="UTF-8">
	<title>Register Confirmation</title>
	</head>
	<body>
	
		<%
			String discovery = session.getAttribute("discovered").toString();
			if(discovery.equals("friends")){
				discovery = "Friends";
			}
			else if(discovery.equals("social_media")){
				discovery = "Social Media";
			}
			else{
				discovery = "others";
			} 
			String birthdate = session.getAttribute("birthdate").toString();
		%>
	
		<h1></h1>
		<p>First Name: <%= session.getAttribute("fname") %></p>
		<p>Last Name: <%= session.getAttribute("lname") %></p>
		<p>Phone Number: <%= session.getAttribute("phone") %></p>
		<p>Email: <%= session.getAttribute("email") %></p>
		<p>App Discovery: <%= discovery %></p>
		<p>Date of Birth: <%= birthdate %></p>
		<p>User Type: <%= session.getAttribute("EorA") %></p>
		<p>Description: <%= session.getAttribute("comments") %></p>
		
		<!-- Submit button for booking -->
		<form action="login" method="post">
			<input type="submit">
		</form>
		
		<form action="Index.jsp">
			<input type="submit" value="Back">
		</form>
		
	</body>
</html>