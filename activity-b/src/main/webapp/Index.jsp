<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>JSP Creating Dynamic Content Activity-B</title>
		<style>
			div {
				margin-top: 5px;
				margin-bottom: 5px;
			}
			
			#container{
				display: flex;
				flex-direction: column;
				justify-content: center;
				align-items: center;
			}
			
			div > input, div > select, div > textarea{
				width: 96%;
			}
			
		</style>
	</head>
	<body>
			
		<div id="container">
		
		<h1>Welcome to Servlet Job Finder!</h1>
		
			<form action="register" method="post">
				<!-- Name -->
				<div>
					<label for="fname">First Name</label>
					<input type="text" name="fname" required>
				</div>
				
				<!-- Phone Number -->
				<div>
					<label for="lname">Last Name</label>
					<input type="text" name="lname" required>
				</div>
				<div>
					<label for="phone">Phone</label>
					<input type="tel" name="phone" required>
				</div>
				<!-- Email Address -->
				<div>
					<label for="email">Email</label>
					<input type="email" name="email" required>
				</div>
				
				<!-- Vehicle Choice -->
				<fieldset>
					<legend>How did you discover the app?</legend>
					<!-- Taxi -->
					<input type="radio" id="friends" name="discovery" value="friends" required>
					<label for="friends">Friends</label>
					<br>
					<!-- Four Seater -->
					<input type="radio" id="social_media" name="discovery" value="social_media" required>
					<label for="social_media">Social Media</label>
					<br>
					<!-- Six Seater -->
					<input type="radio" id="others" name="discovery" value="others" required>
					<label for="others">Others</label>
				</fieldset>
				
				<!-- Pickup Time -->
				<div>
					<label for="birthdate">Date of Birth</label>
					<input type="date" name="birthdate" required>
				</div>
				
				<!-- Pickup Location -->
				<div>
					<label for="userType">Are you an Employer or applicant</label>
					<select id="EorA" name="userType">
						<option value="" selected="selected">Select One</option>
						<option value="applicant">Applicant</option>
						<option value="employee">Employee</option>
					</select>
				</div>
				<div>
					<label for="comments">Profile Description</label>
					<textarea name="comments" maxlength="500"></textarea>
				</div>
				<button>Register</button>
			</form>
		</div>
	</body>
</html>
