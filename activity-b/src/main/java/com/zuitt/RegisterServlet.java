package com.zuitt;

import java.io.IOException;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

@WebServlet("/register")
public class RegisterServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3834552629113201265L;

	public void init() throws ServletException{
		System.out.println("**********************");
		System.out.println("RegisterServlet has been Iniatialized");
		System.out.println("**********************");
	}

public void doPost(HttpServletRequest req, HttpServletResponse res) throws IOException{
		
		String fname = req.getParameter("fname");
		String lname = req.getParameter("lname");
		String email = req.getParameter("email");
		String phone = req.getParameter("phone");
		String discovered = req.getParameter("discovery");
		String birthdate = req.getParameter("birthdate");
		String EorA = req.getParameter("userType");
		String comments = req.getParameter("comments");
		
		//store all the date from the form into the session
		HttpSession session = req.getSession();
		
		session.setAttribute("fname", fname);
		session.setAttribute("lname", lname);
		session.setAttribute("email", email);
		session.setAttribute("phone", phone);
		session.setAttribute("discovered", discovered);
		session.setAttribute("birthdate", birthdate);
		session.setAttribute("EorA", EorA);
		session.setAttribute("comments", comments);
		
		res.sendRedirect("register.jsp");
	}
	
	
	
	public void destroy() {
		System.out.println("**********************");
		System.out.println("RegisterServlet has been destoyed");
		System.out.println("**********************");
	}

}
